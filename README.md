# Obnam benchmark results

This repository contains result files from running the
`obnam-benchmark` tool against the benchmark specifications in the
`obnam-benchmark-specs` repository. A CI job will produce reports from
the result files in this repository, and publish them on a website.

Website is at <https://doc.obnam.org/obnam-benchmark-results/>
