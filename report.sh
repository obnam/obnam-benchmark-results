#!/bin/bash

set -euo pipefail

TITLE="Obnam benchmarks"
obnam-benchmark report "$@" |
	pandoc - -o obnam-benchmark.html --standalone -H report.css --toc --metadata title="$TITLE"
